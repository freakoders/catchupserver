from googleplaces import GooglePlaces, types, lang

YOUR_API_KEY = 'AIzaSyBMc_gZ3tHWU6FrPmm4ZPX_tzBtSdNxFYA'

google_places = GooglePlaces(YOUR_API_KEY)

# You may prefer to use the text_search API, instead.
query_result = google_places.nearby_search(
        location='Pune, India', keyword='Chinese',
        radius=20000, types=[types.TYPE_FOOD])

if not query_result:
    print "No result"

if query_result.has_attributions:
    print query_result.html_attributions

i=1
for place in query_result.places:
    # Returned places from a query are place summaries.
    print '******************',i,'************************'
    i=i+1
    print place.name
    #print place.geo_location
    #print place.reference

    # The following method has to make a further API call.
    place.get_details()
    # Referencing any of the attributes below, prior to making a call to
    # get_details() will raise a googleplaces.GooglePlacesAttributeError.
    # print place.details # A dict matching the JSON response from Google.
    print place.local_phone_number
    #print place.international_phone_number
    print place.website
    #print place.url

    # Getting place photos
    '''	
    for photo in place.photos:
        # 'maxheight' or 'maxwidth' is required
        photo.get(maxheight=500, maxwidth=500)
        # MIME-type, e.g. 'image/jpeg'
        photo.mimetype
        # Image URL
        photo.url
        # Original filename (optional)
        photo.filename
        # Raw image data
        photo.data
   '''
