import googlemaps


class etaUtility:
	def __init__(self):
		self.gmaps=googlemaps.Client("AIzaSyCVlfA0Gc8C6X5m-U9MTtoBC_tVRE9vINw")
		
	def getETA(self,venueLat,venueLon,userLat,userLon):
		print "here"
		if(venueLat=="" or venueLon==""):
			return -1,-1
		print "valid venue"

		a=float(venueLat)
		b=float(venueLon)
		
		c=float(userLat)
		d=float(userLon)


		if(a==600 or b==600 or c==600 or d==600):
			return -1,-1

		print "valid userLocation"
			
		directions_result = self.gmaps.distance_matrix((a,b),(c,d))
		return directions_result['rows'][0]['elements'][0]['duration']['value'],directions_result['rows'][0]['elements'][0]['distance']['value']
	
	def getETADist(self,venueLat,venueLon,userLat,userLon):
		if(venueLat=="" or venueLon==""):
			return -1

		
		a=float(venueLat)
		b=float(venueLon)
		
		c=float(userLat)
		d=float(userLon)
		if(a==600 or b==600 or c==600 or d==600):
			return -1
		directions_result = self.gmaps.distance_matrix((a,b),(c,d))
		return directions_result['rows'][0]['elements'][0]['distance']['value']

	#NEVER USE THIS FUNCTION, FOR TESTING PURPOSE ONLY
	def printALL(self,ll1,ll2):
		directions_result = self.gmaps.distance_matrix(ll1,ll2)
		print directions_result

x=etaUtility()
print x.getETADist("","","600","600")
# x.printALL("18.543543,73.423432","18.4343,73.3232")
