#has_key is depricated in python 3.0

import sys, json, xmpp, random, string, urllib2
import threading
from MongoTalker import *
from time import sleep

SERVER = 'gcm.googleapis.com'
PORT = 5235	#5236 for testing; 5235 for normal
'''
#OLD CREDS GcmLocater App
USERNAME = "274953604138"	#"Your GCM Sender Id(Project No)"
PASSWORD = "AIzaSyBMc_gZ3tHWU6FrPmm4ZPX_tzBtSdNxFYA"	#"API Key"
'''

USERNAME="233688222995"
PASSWORD="AIzaSyCFh-KSnx93OHoqhgGBOax2rh28DsI2pME"




MONGODB_HOST='localhost'
MONGODB_PORT=27017
MONGODB_DB='tpdb'

unacked_messages_quota = 100
send_queue = []

# Return a random alphanumerical id
def random_id():
  rid = ''
  for x in range(8): rid += random.choice(string.ascii_letters + string.digits)
  return rid


def sendResponse(jsonData):
	print "inside response thread"
	send_queue.append({'to': str(jsonData["to"]),
                   'message_id': str(random_id()),
		   #'messageType' : '2',  #not here, our message type has to be inside response
		   'data': jsonData
		})

def sendNotification(jsonData):
	print "inside notification thread"
	gcmRegIdList=jsonData["gcmRegIdList"]
	del jsonData["gcmRegIdList"]
	for gcmRegId in gcmRegIdList:
		send_queue.append({'to': str(gcmRegId),
		           'message_id': str(random_id()),
			   #'messageType' : '2',  #not here, our message type has to be inside response
			   'data': jsonData		############## CHANGE THIS #################
			})


def sendAck(jsonData):
	print "inside Acknowledge thread"
	send_queue.append({'to': str(jsonData["to"]),
               'message_id': str(random_id()),
	   #'messageType' : '2',  #not here, our message type has to be inside response
	   'data': jsonData
	})




def message_callback(session, message):
  global unacked_messages_quota
  gcm = message.getTags('gcm')
  print "****** Some message received!"
  if gcm:
    gcm_json = gcm[0].getData()
    msg = json.loads(gcm_json)
    if not msg.has_key('message_type'):
      print "*** Msg Recieved from :",msg['from']	
      # Acknowledge the incoming message immediately.
      send({'to': msg['from'],
            'message_type': 'ack',
            'message_id': msg['message_id']})
	
      # Queue a response back to the server.
      if msg.has_key('from'):
	print "***********"
	print msg
	postURL=msg['data']['url']
	del msg['data']['url']
	postData=msg['data']	
	req = urllib2.Request('http://127.0.0.1:5000'+postURL)
	req.add_header('Content-Type', 'application/json')	
	response = urllib2.urlopen(req, json.dumps(postData))
	#response is an instant if dictionary is dumped
	#response=list(response)[0]	#dont know why this works
	
		


	print "got response from Flask"	
	try:
		stringData=response.read()	
		jsonData=json.loads(stringData)		#string to dictionary
		jsonData['to']=msg['from']
		
		if "messageType" in jsonData:		
			if jsonData["messageType"]=="2" or jsonData["messageType"]=="3" or jsonData["messageType"]=="4" or jsonData["messageType"]=="6" or jsonData["messageType"]=="7" or jsonData["messageType"]=="8" or jsonData["messageType"]=="9":			
				#print "inside if for sending response"
				thread=threading.Thread(target=sendResponse,args=[jsonData])
				thread.start()
				
			elif jsonData["messageType"]=="101" or jsonData["messageType"]=="102":		#sending notification for new event
				thread=threading.Thread(target=sendNotification,args=[jsonData])
				thread.start()	


			elif jsonData["messageType"]=="301" or jsonData["messageType"]=="302":		#sending ACK for init user,contactlist,event etc
				thread=threading.Thread(target=sendAck,args=[jsonData])
				thread.start()	
	except:
		print "Response:",stringData

    elif msg['message_type'] == 'ack' or msg['message_type'] == 'nack':
      unacked_messages_quota += 1

def send(json_dict):
  template = ("<message><gcm xmlns='google:mobile:data'>{1}</gcm></message>")
  client.send(xmpp.protocol.Message(
      node=template.format(client.Bind.bound[0], json.dumps(json_dict))))

def flush_queued_messages():
  global unacked_messages_quota
  while len(send_queue) and unacked_messages_quota > 0:
    send(send_queue.pop(0))
    print "Sent"
    unacked_messages_quota -= 1

def generateThread():
	mongoTalkerObject=MongoTalker(MONGODB_HOST,MONGODB_PORT,MONGODB_DB)
	listOfUsers=mongoTalkerObject.getUser()
	for user in listOfUsers:
		thread=threading.Thread(target=generateMessage,args=[user["gcmRegId"]])
		thread.start()

def generateMessage(regId):
	global send_queue
	while(True):
		print "1"
		sleep(10)
		print "2"
		send_queue.append({'to': regId,
                   'message_id': 'reg_id',
		   'data':{'content':'Location Update Request','messageType' : '1'}
		})
		print "Queued message!"


print "Server started..."
client = xmpp.Client('gcm.googleapis.com', debug=['socket'])
client.connect(server=(SERVER,PORT), secure=1, use_srv=False)
auth = client.auth(USERNAME, PASSWORD)
if not auth:
  print 'Authentication failed!'
  sys.exit(1)

client.RegisterHandler('message', message_callback)
'''
thread=threading.Thread(target=generateThread)
thread.start()
'''
while True:
  client.Process(1)
  flush_queued_messages()
